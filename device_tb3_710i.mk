DEVICE_PATH := device/lenovo/tb3_710i

### TODO: not sure of what these inherits do yet
# inherit generic full base makefile I guess
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base.mk)
# inherit languages makefile
$(call inherit-product, $(SRC_TARGET_DIR)/product/languages_full.mk)
# inherit the common gps makefile
$(call inherit-product, device/common/gps/gps_as_supl.mk)
# inheriting common stuff
$(call inherit-product, build/target/product/full.mk)

# inheriting dalvik heap config
$(call inherit-product, frameworks/native/build/tablet-7in-hdpi-1024-dalvik-heap.mk)
# inheriting 2048 phone hwui memory makefile cuz native doesnt exist
$(call inherit-product-if-exists, frameworks/native/build/phone-xxhdpi-2048-hwui-memory.mk)

# TODO: add the vendor file
#$(call inherit-product-if-exists, vendor/lenovo/tb3_710i/tb3_710i-vendor.mk)

# screen density I guess (pixel density i.e., dpi is 160 (approx) if I'm right about this)
PRODUCT_AAPT_CONFIG := ldpi mdpi hdpi
PRODUCT_AAPT_PREF_CONFIG := mdpi

### TODO: unknown prebuilt kernel stuff
# checking prebuilt kernel
ifeq ($(TARGET_PREBUILT_KERNEL),)
	LOCAL_KERNEL := $(DEVICE_PATH)/zImage
else
	LOCAL_KERNEL := $(TARGET_PREBUILT_KERNEL)
endif
# coping kernel?
PRODUCT_COPY_FILES += \
	$(LOCAL_KERNEL):kernel

### TODO: add the overlay directories
#DEVICE_PACKAGE_OVERLAYS += $(DEVICE_PATH)/overlay
#$(call inherit-product, $(DEVICE_PATH)/overlay-binaries/overlay-binaries.mk)

### TODO: device settings device settings and configuration stuff?
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.camera.xml:system/etc/permissions/android.hardware.camera.xml \
	frameworks/native/data/etc/android.hardware.audio.low_latency.xml:system/etc/permissions/android.hardware.audio.low_latency.xml \
	frameworks/native/data/etc/tablet_core_hardware.xml:system/etc/permissions/tablet_core_hardware.xml \
	frameworks/native/data/etc/android.hardware.camera.flash-autofocus.xml:system/etc/permissions/android.hardware.camera.flash-autofocus.xml \
	frameworks/native/data/etc/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
	frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
	frameworks/native/data/etc/android.hardware.wifi.direct.xml:system/etc/permissions/android.hardware.wifi.direct.xml \
	frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:system/etc/permissions/android.hardware.sensor.accelerometer.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
	frameworks/native/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml \
	frameworks/native/data/etc/android.hardware.usb.host.xml:system/etc/permissions/android.hardware.usb.host.xml \
	frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml \
	frameworks/native/data/etc/android.hardware.bluetooth_le.xml:system/etc/permissions/android.hardware.bluetooth_le.xml \
	frameworks/native/data/etc/handheld_core_hardware.xml:system/etc/permissions/handheld_core_hardware.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.xml:system/etc/permissions/android.hardware.touchscreen.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.distinct.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.distinct.xml \
	frameworks/native/data/etc/android.software.webview.xml:system/etc/permissions/android.software.webview.xml \
	frameworks/native/data/etc/android.software.sip.xml:system/etc/permissions/android.software.sip.xml \
	frameworks/native/data/etc/android.hardware.bluetooth.xml:system/etc/permissions/android.hardware.bluetooth.xml \
	frameworks/native/data/etc/android.hardware.location.xml:system/etc/permissions/android.hardware.location.xml \
	frameworks/native/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml
#	frameworks/native/data/etc/android.hardware.wifi.aware.xml:system/etc/permissions/android.hardware.wifi.aware.xml 
PRODUCT_PACKAGES += \
	libwpa_client \
	hostapd \
	dhcpcd.conf \
	wpa_supplicant \
        wpa_supplicant.conf \
	libbt-vendor \
	libxlog
PRODUCT_PROPERTY_OVERRIDES += \
	persist.sys.strictmode.visual=0 \
	debug.atrace.tags.enableflags=802922 \
	persist.sys.strictmode.disable=1 \
	ro.hwui.texture_cache_size=24 \
	ro.hwui.layer_cache_size=16 \
	ro.hwui.gradient_cache_size=0.5 \
	ro.hwui.patch_cache_size=128 \
	ro.hwui.path_cache_size=4 \
	ro.hwui.shape_cache_size=1 \
	ro.hwui.drop_shadow_cache_size=2 \
	ro.hwui.r_buffer_cache_size=2 \
	ro.hwui.texture_cache_flush_rate=0.6 \
	ro.hwui.text_small_cache_width=1024 \
	ro.hwui.text_small_cache_height=512 \
        ro.hwui.text_large_cache_width=2048 \
        ro.hwui.text_large_cache_height=1024 \
        ro.zygote.disable_gl_preload=false \
        hwui.text_gamma_correction=lookup3 \
        hwui.use_gpu_pixel_buffers=true \
        hwui.text_gamma.white_threshold=192 \
        hwui.text_gamma.black_threshold=64 \
        hwui.text_gamma=1.4
# no RIL
PRODUCT_PROPERTY_OVERRIDES += \
	keyguard.no_require_sim=1 \
	ro.radio.use-ppp=no \
        ro.config.nocheckin=yes \
	ro.radio.noril=1 \
        ro.carrier=wifi-only \
	persist.radio.noril=1
# Audio
PRODUCT_PROPERTY_OVERRIDES += aaudio.mmap_policy=2
PRODUCT_PROPERTY_OVERRIDES += aaudio.mmap_exclusive_policy=2
# To impliment split screen in future
PRODUCT_PROPERTY_OVERRIDES += ro.config.low_ram=false
# Audio
PRODUCT_PACKAGES += \
	audio.a2dp.default \
	audio.usb.default \
	audio.r_submix.default \
	libtinyalsa \
	tinymix
# Bluetooth
PRODUCT_PACKAGES += \
        bluetooth.default
# Power
PRODUCT_PACKAGES += \
	power.default
# network
PRODUCT_PACKAGES += \
        netd
# IPv6 tethering
PRODUCT_PACKAGES += \
	ebtables \
        ethertypes
# root access
PRODUCT_PACKAGES += \
        su
# TextClassifier smart selection model files
PRODUCT_PACKAGES += \
	textclassifier.smartselection.bundle1
ADDITIONAL_DEFAULT_PROPERTIES += ro.secure=0 \
				 ro.adb.secure=0 \
				 ro.allow.mock.location=1 \
				 ro.debuggable=1 \
				 persist.sys.usb.config=mtp,adb

# overriding this after every inherit
PRODUCT_NAME := lineage_tb3_710i
PRODUCT_BUILD_PROP_OVERRIDES += BUILD_UTC_DATE=0
PRODUCT_CHARACTERISTICS := tablet
PRODUCT_BRAND := lenovo
PRODUCT_MODEL := tb3_710i
PRODUCT_MANUFACTURER := lenovo
PRODUCT_DEVICE := tb3_710i

# boot animation
TARGET_SCREEN_HEIGHT := 1024
TARGET_SCREEN_WIDTH := 600


