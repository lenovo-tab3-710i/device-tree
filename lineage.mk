# TODO: complete that full_device.mk file
$(call inherit-product, device/lenovo/tb3_710i/full_tb3_710i.mk)

## Inherit from the common Open Source product configuration
# the telephony configuration of the device
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base_telephony.mk)

## Inherit common CM phone.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# inherit tablet wifionly makefile
$(call inherit-product, vendor/cm/config/common_full_tablet_wifionly.mk)

# TODO: what is this
#PRODUCT_CMS_CLIENT_BASE := something

# device fingerint for authentication reason?
BUILD_FINGERPRINT=Ixion/lineage_P350/P350:6.0.1/MOI10E/e54767a8a3:userdebug/test-keys \
PRIVATE_BUILD_DESC="lineage_P350-userdebug 6.0.1 MOI10E e54767a8a3 test-keys"

# device metadata variables basically (should come after all inherits)
PRODUCT_NAME := lineage_tb3_710i
PRODUCT_DEVICE := tb3_710i
PRODUCT_BRAND := lenovo
PRODUCT_MODEL := tb3_710i 
PRODUCT_MANUFACTURER := lenovo

